﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace Practice
{
    class HomePage
    {
        private IWebDriver webDriver;
        //Home
        //[FindsBy(How = How.Id, Using = "create_link")]
        //public IWebElement lnkCreate { get; set; }

        //[FindsBy(How = How.Id, Using = "quickSearchInput")]
        //public IWebElement txtSearchIssue { get; set; }

        ////Create new issue pop up
        //[FindsBy(How = How.Id, Using = "issuetype-field")]
        //public IWebElement drpIssueType { get; set; }

        //[FindsBy(How = How.Id, Using = "summary")]
        //public IWebElement txtSummary { get; set; }

        //[FindsBy(How = How.Id, Using = "create-issue-submit")]
        //public IWebElement btnCreateNewIssue { get; set; }



        //public HomePage(IWebDriver webDriver)
        //{
        //    this.webDriver = webDriver;
        //    // I need to use this as its not freaking waiting for my Page to load when I pass the webdriver in the consturctor.
        //    var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(60));
        //    try
        //    {
        //        wait.Until(driver => driver.FindElement(By.XPath("//*[@id='content']/header/div/div[2]/h1[.='A Test Project']")));
        //    }
        //    catch (Exception e)
        //    {
        //        throw new Exception("This is not the home page.");
        //    }
        //    PageFactory.InitElements(webDriver, this);
        //}

        public void OpenHomePage(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
            webDriver.Navigate().GoToUrl("https://jira.atlassian.com/browse/TST");
        }

        public string CreateNewTask(IWebDriver webDriver,string summary)
        {
            try
            {
                this.webDriver = webDriver;
                IWebElement lnkCreate = webDriver.FindElement(By.Id("create_link"));
                IWebElement avatar = webDriver.FindElement(By.XPath("//*[@id='header-details-user-fullname']"));
                //Click Create new issue
                lnkCreate.Click();
                var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(60));
                wait.Until(driver => driver.FindElement(By.Id("issuetype-field")));
                
                //Input required data
                IWebElement drpIssueType = webDriver.FindElement(By.Id("issuetype-field"));
                drpIssueType.Click();
                webDriver.FindElement(By.XPath("//*[@id='issuetype-suggestions']/div/ul/li/*[@title='Task']")).Click();

                Thread.Sleep(TimeSpan.FromSeconds(3));
                webDriver.FindElement(By.XPath("//input[@id='summary']")).SendKeys(summary);

                Thread.Sleep(TimeSpan.FromSeconds(2));
                webDriver.FindElement(By.XPath("//input[@id='create-issue-submit']")).Click();
                Thread.Sleep(TimeSpan.FromSeconds(3));
                //Go to issue navigator
                avatar.Click();
                webDriver.FindElement(By.XPath("//*[@id='set_my_jira_home_issuenav']")).Click();
                Thread.Sleep(TimeSpan.FromSeconds(3));
                string xpathIssueID =
                    String.Format("//*[@id='content']//*[@class='issue-list']//span[.='{0}']/../span[@class='issue-link-key']",summary);

                wait.Until(driver => driver.FindElement(By.XPath(xpathIssueID)));
                Assert.True(webDriver.FindElement(By.XPath(xpathIssueID)).Displayed);
                string issueID = webDriver.FindElement(By.XPath(xpathIssueID)).Text;

                return issueID;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
        }

        public void SearchExistingIssue(IWebDriver webDriver,string issueID)
        {
            if ((String.IsNullOrEmpty(issueID)))
            {
                Console.WriteLine("No issueID");
                Assert.IsNotNullOrEmpty(issueID);
                return;
            }
            else
            {
                try
                {
                    this.webDriver = webDriver;
                    IWebElement txtQuickSearch = webDriver.FindElement(By.XPath("//*[@id='quickSearchInput']"));
                    txtQuickSearch.SendKeys(issueID);
                    txtQuickSearch.SendKeys(Keys.Enter);

                    var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(60));
                    wait.Until(driver => driver.FindElement(By.Id("key-val")));

                    Assert.AreEqual(webDriver.FindElement(By.Id("key-val")).Text, issueID);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    throw ex;
                }
            }
        }

        public void UpdateIssueDescription(IWebDriver webDriver, string issueID,string description)
        {
            this.webDriver = webDriver;
            try
            {
                SearchExistingIssue(webDriver, issueID);
                //Click edit
                webDriver.FindElement(By.XPath("//*[@id='edit-issue']")).Click();
                var wait = new WebDriverWait(webDriver, TimeSpan.FromSeconds(60));
                wait.Until(driver => driver.FindElement(By.Id("description")));

                IWebElement txtDescription = webDriver.FindElement(By.XPath("//textarea[@id='description']"));
                txtDescription.SendKeys(description);
                webDriver.FindElement(By.XPath("//*[@id='edit-issue-submit']")).Click();
                Thread.Sleep(TimeSpan.FromSeconds(4));

                wait.Until(driver => driver.FindElement(By.XPath("//*[@id='description-val']//div[@class='user-content-block']")));

                IWebElement verifyDescription = webDriver.FindElement(By.XPath("//*[@id='description-val']//div[@class='user-content-block']"));
                Assert.AreEqual(verifyDescription.Text,description);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }

            


        }

    }
}
