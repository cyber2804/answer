﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace Practice
{
    public class LoginPage
    {
        private IWebDriver webDriver;
        private HomePage homePage;

        //[FindsBy(How = How.Id, Using = "username")]
        //public IWebElement UserName { get; set; }

        //[FindsBy(How = How.Id, Using = "password")]
        //public IWebElement Password { get; set; }

        //[FindsBy(How = How.Id, Using = "login-submit")]
        //public IWebElement SubmitButton { get; set; }

        //public LoginPage(IWebDriver webDriver)
        //{
        //    this.webDriver = webDriver;
        //    if (!webDriver.Url.Contains("login?"))
        //    {
        //        throw new Exception("This is not the login page");
        //    }
        //    PageFactory.InitElements(webDriver, this);
        //}

        public void OpenLoginPage(IWebDriver webDriver)
        {
            this.webDriver = webDriver;
            webDriver.Navigate().GoToUrl("https://id.atlassian.com/login");
            webDriver.Manage().Window.Maximize();
        }

        public void LoginValid(string username, string password, IWebDriver driver)
        {
            try
            {
                IWebElement UserName = driver.FindElement(By.Id("username"));
                IWebElement Password = driver.FindElement(By.Id("password"));
                IWebElement SubmitButton = driver.FindElement(By.Id("login-submit"));

                UserName.SendKeys(username);
                Password.SendKeys(password);
                SubmitButton.Click();
                IWebElement validLogin = driver.FindElement(By.XPath("//*[@id='email']"));

                Assert.AreEqual(validLogin.Text,username);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw ex;
            }
            
            
            
        }
    }
}
