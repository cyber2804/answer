﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace Practice
{
    [TestFixture]
    public class RunTest
    {
        private IWebDriver driver;
        private HomePage homePage ;
        private LoginPage loginPage;
        private string issueID ="";

        //data
        public string newSummary = "Task 1";
        public string newDescription = "new description";

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            try
            {
                driver = new FirefoxDriver();
                loginPage = new LoginPage();
                loginPage.OpenLoginPage(driver);
                loginPage.LoginValid("cyber2804@gmail.com","password1@",driver);
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            try
            {
                driver.Quit();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        [Test]
        [Description("Create new issue")]
        public void CreateNewIssue()
        {
            try
            {
                homePage = new HomePage();
                //Create new issue - task
                homePage.OpenHomePage(driver);
                issueID = homePage.CreateNewTask(driver, newSummary);

                //Update existing issue
                homePage.UpdateIssueDescription(driver, issueID, newDescription);

                //Search existing issue
                homePage.SearchExistingIssue(driver, issueID);
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        //[Test]
        //[Description("Updating an existing issue")]
        //public void UpdateExistingIssue()
        //{
        //    try
        //    {
        //        homePage = new HomePage();
        //        homePage.OpenHomePage(driver);
                
        //    }

        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex);
        //    }
        //}

        //[Test]
        //[Description("Search an existing issue")]
        //public void SearchExistingIssue()
        //{
        //    try
        //    {
        //        homePage = new HomePage();
        //        homePage.OpenHomePage(driver);
        //        homePage.SearchExistingIssue(driver, issueID);
                
        //    }

        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex);
        //    }
        //}
    }
}
